package android.widget;

import android.content.Context;
import android.util.AttributeSet;

public abstract class AbsSpinner extends AdapterView {

	public AbsSpinner(Context context) {
		super(context);
	}

	public AbsSpinner(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
	}

}