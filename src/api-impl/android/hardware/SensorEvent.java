package android.hardware;

public class SensorEvent {

	public final float[] values;

	public SensorEvent(float[] values) {
		this.values = values;
	}
	
}
